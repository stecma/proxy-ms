# PROXY-MS
## Descripción    
Este microservicio será el único microservicio de Back-End accesible desde el exterior de la plataforma. Todas y cada una de las peticiones que se efectúen desde la interfaz del usuario se hará contra él, permitiendo así que el resto de los microservicios no sean visibles desde el exterior de la plataforma. Este servicio además se encargará de:

*	Envío de logs a Elastic.
*	Gestión de las rutas de los microservicios privados, mediante el Zuul de Netflix y con el almacenamiento de las rutas en Consul.

## Prerrequisitos
Para la correcta compilación y ejecución de la aplicación es necesario lo siguiente:

* Tener instalado maven 3 en nuestro equipo
* Tener configurado en el settings.xml de maven el repositorio nexus de globalia para poder acceder a los artefactos necesario para la compilación del proyecto
* Tener instalado y configurado java 11
    
## Construcción y ejecución del servicio 
 La construcción del servicio está basada en `maven`. Para la correcta compilación es necesario ejecutar el siguiente comando:   

```bash prompt> clean install```

Para la compilación en local es necesario ejecutar el profile local para que no se compilen unas librerías que solo afectan a openshift y producen errores al levantar la aplicación en local:

```bash prompt> clean install -Plocal``` 

    
Tras ejecutar en el comando anterior, `maven` creará en la carpeta target/classes el jar del api "proxy-{VERSION}-exec.jar" así como las dependencias necesarias para la correcta ejecución del api (para ello se usan maven-jar-plugin y maven-dependency-plugin).    
    
## Puesta en marcha del API
Para levantar el API se debe ejecutar el siguiente comando:

```bash prompt> java -jar target/classes/proxy-{VERSION}-exec.jar --spring.profiles.active=local --consul.ip={{consul.ip}}```

Variables necesarias:

| Clave  | Descripción  | Valores posibles |
|---|---|---|
| spring.profiles.active | Perfil activo. | local | 
| consul.ip | Host de consul desde el que se recuperarán todos los endpoints a configurar en el proxy. | Dirección ip | 
    
## Configuración   
El proxy-ms usa las siguientes propiedades de configuración:

### bootstrap.yml
Para configurar la ruta de consul desde la que leer todas las propiedades de la aplicación se usa el archivo bootstrap.yml que contiene las siguientes propiedades:

| Clave  | Descripción  | Valores posibles |
|---|---|---|
| spring.application.name | Nombre de la aplicación | proxy |
| spring.cloud.consul.host | Host de consul | Dirección ip |
| spring.cloud.consul.port | Puesto de consul | Valor numérico |
| spring.cloud.consul.token | Token de acceso a consul | UUID |
| spring.cloud.consul.config.fail-fast | Si no se pueden cargar las propiedades de consul produce una excepción o un warning según el valor configurado | Valor booleano |
| spring.cloud.consul.config.enabled | Habilitar/Deshabilitar la configuración del consul | Valor booleano |
| spring.cloud.consul.config.prefix | Prefijo bajo el que se alojan las propiedades a recuperar | Cadena de texto |
| spring.cloud.consul.config.defaultContext | Contexto por defecto | Cadena de texto |

### application.yml
| Clave  | Descripción  | Valores posibles |
|---|---|---|
| server.error.whitelabel.enabled |  | Valor booleano |
| server.port | Puerto de acceso a los endpoints de la aplicación  | Valor numérico |
| spring.main.banner-mode | Mostrar el banner de spring | Valor booleano |
| spring.main.allow-bean-definition-overriding | Permitir sobreescritura de beans | Valor booleano |
| zuul.host.connect-timeout-millis | Configuración del timeout de conexión de zuul | Milisegundos |
| zuul.host.socket-timeout-millis | Configuración del timeout del socket conexión de zuul | Milisegundos |
| zuul.ribbon.ConnectTimeout | Configuración del timeout de conexión de ribbon | Milisegundos |
| zuul.ribbon.ReadTimeout | Configuración del timeout de lectura de ribbon | Milisegundos |


## Logs
La gestión de logs se hace mediante Filebeat, para ello es necesario añadir el archivo log4j2-spring.xml con el siguiente contenido:


```<?xml version="1.0" encoding="UTF-8"?>
<Configuration>
	<Appenders>
		<Console name="Console" target="SYSTEM_OUT">
			<PatternLayout pattern="{&quot;timestamp&quot;:&quot;%d{ISO8601}&quot;,%m}%n"/>
		</Console>
	</Appenders>

	<Loggers>
		<Root level="info">
			<AppenderRef ref="Console"/>
		</Root>

		<Logger name="com.globalia" level="info"/>
		<Logger name="com.globalia" level="TRACE">
			<AppenderRef ref="Console"/>
		</Logger>
	</Loggers>
</Configuration>```