package com.globalia.infraestructure.config;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.netflix.zuul.filters.ZuulProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.util.StringUtils;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@SuppressWarnings("unused")
@Configuration
@RefreshScope
public class ZuulConfig {

	@Value("${zuul.zuulServices}")
	private String zuulServices;
	@Value("${zuul.loginMs}")
	private String loginMs;
	@Value("${zuul.gestcomMs}")
	private String gestcomMs;

	@Primary
	@Bean(name = "zuulConfigProperties")
	@ConfigurationProperties("zuul")
	public ZuulProperties zuulProperties() {
		Set<String> microservices = new HashSet<>();
		JsonObject json = (JsonObject) JsonParser.parseString(this.zuulServices);
		for (JsonElement service : json.get("services").getAsJsonArray()) {
			microservices.add(service.getAsString());
		}
		return this.getServiceRoutes(microservices);
	}

	private ZuulProperties getServiceRoutes(final Set<String> microservices) {
		ZuulProperties props = null;
		if (!microservices.isEmpty()) {
			Map<String, ZuulProperties.ZuulRoute> map = new HashMap<>();
			for (String service : microservices) {
				JsonObject json = this.getJson(service);
				if (json != null) {
					String url = json.getAsJsonObject().get("url").getAsString();

					for (JsonElement route : json.getAsJsonObject().get("routes").getAsJsonArray()) {
						ZuulProperties.ZuulRoute zuulRoute = new ZuulProperties.ZuulRoute();
						zuulRoute.setId(route.getAsJsonObject().get("id").getAsString());
						zuulRoute.setPath(route.getAsJsonObject().get("path").getAsString());
						zuulRoute.setUrl(url);
						zuulRoute.setServiceId(service);
						zuulRoute.setStripPrefix(false);
						map.put(zuulRoute.getId(), zuulRoute);
					}
				}
			}

			props = new ZuulProperties();
			props.setRoutes(map);
		}
		return props;
	}

	private JsonObject getJson(final String service) {
		String content = this.getContent(service);
		if (StringUtils.hasText(content)) {
			return (JsonObject) JsonParser.parseString(content);
		}
		return null;
	}

	private String getContent(final String service) {
		try {
			Field field = ZuulConfig.class.getDeclaredField(service);
			return (String) field.get(this);
		} catch (NoSuchFieldException | IllegalAccessException ignored) {
			//
		}
		return null;
	}
}