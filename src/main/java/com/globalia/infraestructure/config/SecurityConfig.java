package com.globalia.infraestructure.config;

import com.globalia.enumeration.LogType;
import com.globalia.security.JwtTokenFilterConfigurer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.ldap.authentication.ad.ActiveDirectoryLdapAuthenticationProvider;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;

import java.util.Collections;
import java.util.List;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private JwtTokenFilterConfigurer tokenFilterConfigurer;

	@Value("${spring.application.name}")
	private String appName;
	@Value("${server.port}")
	private String port;
	@Value("${ldap.uri}")
	private String ldapUrl;
	@Value("${ldap.domain}")
	private String ldapDomain;
	@Value("${logLevel}")
	private LogType logLevel;
	@Value("${jwt.secret}")
	private String jwtSecret;
	@Value("${jwt.encryptSecret}")
	private String encryptSecret;
	@Value("${jwt.encryptAlgorithm}")
	private String encryptAlgorithm;
	@Value("${cors.allowedMethods}")
	private List<String> allowedMethods;
	@Value("${cors.allowedOrigins}")
	private List<String> allowedOrigins;
	@Value("${cors.allowedHeaders}")
	private List<String> allowedHeaders;

	@Override
	protected void configure(final HttpSecurity http) throws Exception {
		// Disable CSRF (cross site request forgery)
		if (this.allowedOrigins != null && !this.allowedOrigins.isEmpty()) {
			http.cors().configurationSource(this.corsConfigSource());
		}
		http.csrf().disable();

		// No session will be created or used by spring security
		http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

		// Entry points
		http.authorizeRequests()
				.antMatchers("/**/login", "/**/reset", "/**/health", "/**/reload").permitAll()
				// Disallow everything else..
				.anyRequest().authenticated();

		// Apply JWT
		this.tokenFilterConfigurer.init(this.logLevel, this.jwtSecret, this.encryptSecret, this.encryptAlgorithm, this.appName, this.port);
		http.apply(this.tokenFilterConfigurer);
	}

	@Override
	public AuthenticationManager authenticationManager() {
		ActiveDirectoryLdapAuthenticationProvider provider = new ActiveDirectoryLdapAuthenticationProvider(this.ldapDomain, this.ldapUrl);
		provider.setConvertSubErrorCodesToExceptions(true);
		provider.setUseAuthenticationRequestCredentials(true);

		return new ProviderManager(Collections.singletonList(provider));
	}

	private CorsConfigurationSource corsConfigSource() {
		CorsConfiguration corsConfig = new CorsConfiguration();
		corsConfig.setAllowCredentials(true);
		this.allowedOrigins.forEach(corsConfig::addAllowedOriginPattern);
		this.allowedHeaders.forEach(corsConfig::addAllowedHeader);
		this.allowedMethods.forEach(corsConfig::addAllowedMethod);

		return request -> corsConfig;
	}
}