package com.globalia.infraestructure.advice;

import com.globalia.api.ApiRS;
import com.globalia.advice.HandlerAdvice;
import com.globalia.enumeration.LogType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
class GlobalExceptionHandler {

	@Autowired
	private HandlerAdvice handler;

	@Value("${spring.application.name}")
	private String appName;
	@Value("${server.port}")
	private String port;
	@Value("${logLevel}")
	private LogType logLevel;

	/**
	 * Handler exception.
	 *
	 * @param request   Request.
	 * @param exception Exception.
	 * @return Error message.
	 */
	@ExceptionHandler(Exception.class)
	ResponseEntity<ApiRS> handleException(final HttpServletRequest request, final Exception exception) {
		return new ResponseEntity<>(this.handler.handleException(request, exception, this.appName, this.port, this.logLevel), HttpStatus.INTERNAL_SERVER_ERROR);
	}
}