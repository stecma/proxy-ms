package com.globalia.application;

import com.globalia.monitoring.Monitor;
import com.globalia.monitoring.MonitorHandler;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.stereotype.Component;
import org.springframework.util.StreamUtils;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Set;

@Log4j2
@Component
public class MetricsPreFilter extends ZuulFilter {

	@Value("${spring.application.name}")
	private String appName;
	@Value("${server.port}")
	private String port;

	@Autowired
	private MonitorHandler handler;

	public String filterType() {
		return FilterConstants.PRE_TYPE;
	}

	public int filterOrder() {
		return FilterConstants.PRE_DECORATION_FILTER_ORDER + 1;
	}

	public boolean shouldFilter() {
		RequestContext ctx = RequestContext.getCurrentContext();
		boolean result = false;
		if (StringUtils.hasText(ctx.getRequest().getRequestURI()) && ctx.getRequest().getRequestURI().startsWith("/api")) {
			result = ctx.getRouteHost() != null && ctx.sendZuulResponse();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public Object run() {
		try {
			RequestContext ctx = RequestContext.getCurrentContext();
			Set<String> headers = (Set<String>) ctx.get("ignoredHeaders");
			if (headers != null) {
				headers.remove("authorization");
			}

			String body = this.getBody(ctx);
			String route = ctx.getRequest().getRequestURI();
			String dftUri = (String) ctx.get("requestURI");
			if (StringUtils.hasText(dftUri) && !"/".equals(dftUri) && route.contains(dftUri)) {
				route = route.replace(dftUri, "");
			}

			Monitor monitor = this.handler.create(body, this.appName, this.port);
			monitor.setRoute(dftUri);

			ctx.setRequest(HttpRQWrapper.getInstance().modifyRequest(ctx.getRequest(), StringEscapeUtils.unescapeJava(this.buildRQ(body, (String) ctx.get("proxy"), monitor))));
			ctx.setRouteHost(new URL(ctx.getRouteHost().toString() + route));
		} catch (IOException | NullPointerException ex) {
			MetricsPreFilter.log.error(ex);
		}
		return null;
	}

	private String buildRQ(final String body, final String proxy, final Monitor monitor) {
		StringBuilder json = new StringBuilder("{");
		if (StringUtils.hasText(body)) {
			String model = proxy;
			if (model.lastIndexOf('_') != -1) {
				model = proxy.substring(0, proxy.lastIndexOf('_'));
			}
			json.append(String.format("\"%s\":%s,", model, body));
		}
		json.append(String.format("\"mnt\":%s}", this.handler.toJson(monitor)));
		return json.toString().replaceAll("\n", "").replaceAll("\t", "");
	}

	private String getBody(final RequestContext ctx) throws IOException {
		InputStream in = (InputStream) ctx.get("requestEntity");
		if (in == null) {
			in = ctx.getRequest().getInputStream();
		}
		return StreamUtils.copyToString(in, StandardCharsets.UTF_8);
	}
}