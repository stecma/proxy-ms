package com.globalia.application;

import com.netflix.zuul.http.HttpServletRequestWrapper;
import com.netflix.zuul.http.ServletInputStreamWrapper;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

@Log4j2
@Component
public class HttpRQWrapper {

	private static HttpRQWrapper instance;

	private HttpRQWrapper() {
	}

	public static HttpRQWrapper getInstance() {
		if (HttpRQWrapper.instance == null) {
			HttpRQWrapper.instance = new HttpRQWrapper();
		}
		return HttpRQWrapper.instance;
	}

	public HttpServletRequestWrapper modifyRequest(final HttpServletRequest request, final String body) {
		return new HttpServletRequestWrapper(request) {
			private final Charset utf8 = StandardCharsets.UTF_8;

			@Override
			public byte[] getContentData() {
				return body.getBytes(this.utf8);
			}

			@Override
			public int getContentLength() {
				return body.getBytes().length;
			}

			@Override
			public long getContentLengthLong() {
				return body.getBytes().length;
			}

			@Override
			public BufferedReader getReader() {
				return new BufferedReader(new InputStreamReader(new ByteArrayInputStream(body.getBytes(this.utf8))));
			}

			@Override
			public ServletInputStream getInputStream() {
				return new ServletInputStreamWrapper(body.getBytes(this.utf8));
			}
		};
	}
}