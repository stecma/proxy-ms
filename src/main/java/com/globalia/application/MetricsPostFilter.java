package com.globalia.application;

import com.globalia.Context;
import com.globalia.api.ApiRS;
import com.globalia.configuration.TokenConfig;
import com.globalia.enumeration.ErrorLayer;
import com.globalia.enumeration.LogType;
import com.globalia.exception.Error;
import com.globalia.json.JsonHandler;
import com.globalia.monitoring.Log;
import com.globalia.monitoring.Monitor;
import com.globalia.monitoring.Stat;
import com.globalia.security.JwtTokenProvider;
import com.netflix.util.Pair;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.StreamUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.zip.GZIPInputStream;

@Log4j2
@Component
@RefreshScope
public class MetricsPostFilter extends ZuulFilter {

	private static final int BUFFER_SIZE = 1024;
	private static final String AUTH_HEADER = "Authorization";
	private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss");

	@Autowired
	private JwtTokenProvider tokenProvider;
	@Autowired
	private JsonHandler jsonHandler;
	@Autowired
	private Context context;

	@Value("${spring.application.name}")
	private String appName;
	@Value("${server.port}")
	private String port;
	@Value("${jwt.secret}")
	private String jwtSecret;
	@Value("${jwt.expirationInMs}")
	private int jwtExpirationInMs;
	@Value("${jwt.expirationAdminInMs}")
	private int jwtExpirationAdminInMs;
	@Value("${showLogs}")
	private boolean showLogs;
	@Value("${showStats}")
	private boolean showStats;
	@Value("${sendLogs}")
	private boolean sendLogs;
	@Value("${sendStats}")
	private boolean sendStats;
	@Value("${logsFormat}")
	private String logsFormat;
	@Value("${logLevel}")
	private LogType logLevel;

	public String filterType() {
		return FilterConstants.POST_TYPE;
	}

	public int filterOrder() {
		return FilterConstants.SIMPLE_HOST_ROUTING_FILTER_ORDER - 1;
	}

	public boolean shouldFilter() {
		return true;
	}

	public Object run() {
		ApiRS response = this.getResponse();
		if (response != null) {
			HttpServletResponse servletResponse = RequestContext.getCurrentContext().getResponse();
			try (OutputStream outStream = servletResponse.getOutputStream()) {
				if (response.getMonitor() != null) {
					response.getMonitor().setEndDate(System.currentTimeMillis());
					this.sendMonitor(response);
					this.showMonitor(response);
				}
				if (!RequestContext.getCurrentContext().getRequest().getRequestURI().contains("health")) {
					this.addHeader(response, servletResponse);
				}
				RequestContext.getCurrentContext().setResponseStatusCode(response.getStatus());

				servletResponse.setCharacterEncoding("UTF-8");
				this.writeResponse(new ByteArrayInputStream(this.jsonHandler.toJson(response).getBytes(StandardCharsets.UTF_8)), outStream);
			} catch (IOException jpe) {
				RequestContext.getCurrentContext().setResponseBody(String.format("{\"status\":\"%s\", \"response\": { \"uri\":\"%s\", \"message\":\"%s\", \"errorType\":\"PROXY_LAYER\", \"timestamp\":\"%s\"}}", HttpStatus.INTERNAL_SERVER_ERROR.value(), RequestContext.getCurrentContext().getRequest().getRequestURI(), jpe.getMessage(), this.sdf.format(new Date())));
				RequestContext.getCurrentContext().setResponseStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
			}
		}
		return null;
	}

	private void writeResponse(final InputStream zin, final OutputStream out) throws IOException {
		byte[] bytes = new byte[BUFFER_SIZE];
		int bytesRead;
		while ((bytesRead = zin.read(bytes)) != -1) {
			out.write(bytes, 0, bytesRead);
			out.flush();
		}
	}

	private void addHeader(final ApiRS response, final HttpServletResponse servletResponse) {
		if (this.isStatusAccepted(response.getStatus()) && RequestContext.getCurrentContext().getZuulResponseHeaders() != null) {
			boolean found = false;
			for (Pair<String, String> header : RequestContext.getCurrentContext().getZuulResponseHeaders()) {
				servletResponse.addHeader(header.first(), header.second());
				if (MetricsPostFilter.AUTH_HEADER.equals(header.first())) {
					found = true;
				}
			}

			if (!found) {
				TokenConfig tokenConfig = new TokenConfig();
				tokenConfig.setSecret(Base64.getEncoder().encodeToString(this.jwtSecret.getBytes()));
				tokenConfig.setExpirationAdminInMs(this.jwtExpirationAdminInMs);
				tokenConfig.setExpirationInMs(this.jwtExpirationInMs);

				servletResponse.addHeader("Access-Control-Expose-Headers", MetricsPostFilter.AUTH_HEADER);
				servletResponse.addHeader(MetricsPostFilter.AUTH_HEADER, this.tokenProvider.refreshExpiration(RequestContext.getCurrentContext().getRequest().getHeader(MetricsPostFilter.AUTH_HEADER), tokenConfig, null));
			}
		}
	}

	private ApiRS getResponse() {
		ApiRS response = null;
		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			int read;
			byte[] buff = new byte[MetricsPostFilter.BUFFER_SIZE];
			if (RequestContext.getCurrentContext().getResponseDataStream() != null) {
				while ((read = RequestContext.getCurrentContext().getResponseDataStream().read(buff)) != -1) {
					bos.write(buff, 0, read);
				}

				if (bos.size() > 0) {
					boolean responseGZipped = RequestContext.getCurrentContext().getResponseGZipped();
					InputStream zin;
					if (responseGZipped) {
						zin = new GZIPInputStream(new ByteArrayInputStream(bos.toByteArray()));
					} else {
						zin = new ByteArrayInputStream(bos.toByteArray());
					}

					String content = StreamUtils.copyToString(zin, StandardCharsets.UTF_8);
					response = (ApiRS) this.jsonHandler.fromJson(content, ApiRS.class);
				}
				bos.close();
			}
		} catch (IOException io) {
			Error err = new Error(io.getMessage(), ErrorLayer.PROXY_LAYER);
			err.setUri(RequestContext.getCurrentContext().getRequest().getRequestURI());

			Monitor monitor = this.tokenProvider.getHandler().create(null, this.appName, this.port);
			this.tokenProvider.getHandler().addErrorLog(monitor, Thread.currentThread().getStackTrace()[2].getClassName(), Thread.currentThread().getStackTrace()[2].getMethodName(), Thread.currentThread().getStackTrace()[2].getLineNumber(), Arrays.toString(io.getStackTrace()), this.logLevel);

			response = new ApiRS();
			response.setResponse(err);
			response.setMonitor(monitor);
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		}
		return response;
	}

	private boolean isStatusAccepted(final int status) {
		boolean result = false;
		if (status != HttpStatus.INTERNAL_SERVER_ERROR.value() && status != HttpStatus.UNAUTHORIZED.value() && status != HttpStatus.BAD_REQUEST.value()) {
			result = true;
		}
		return result;
	}

	private void showMonitor(final ApiRS response) {
		if (!this.showLogs && !this.showStats) {
			response.setMonitor(null);
		} else {
			if (!this.showLogs) {
				response.getMonitor().setLogs(null);
			}
			if (!this.showStats) {
				response.getMonitor().setStats(null);
			}
		}
	}

	private void sendMonitor(final ApiRS response) {
		Set<Log> logs = response.getMonitor().getLogs();
		Set<Stat> stats = response.getMonitor().getStats();
		String uuID = response.getMonitor().getCorrelationID();
		String route = response.getMonitor().getRoute();
		long timestamp = response.getMonitor().getTimestamp();
		long endDate = response.getMonitor().getEndDate();
		new Thread(() -> this.send(logs, stats, uuID, route, timestamp, endDate)).start();
	}

	private void send(final Set<Log> logs, final Set<Stat> stats, final String uuid, final String route, final long timestamp, final long endDate) {
		if (this.sendStats && stats != null) {
			this.sendStats(stats, uuid, route, timestamp, endDate);
		}

		if (this.sendLogs && logs != null) {
			for (Log log : this.tokenProvider.getHandler().sortedLogs(logs)) {
				MetricsPostFilter.log.info(this.logsFormat, this.context.getEnvironment(), log.getType().toString(), uuid, log.getService(), route, log.getHost(), log.getClazz(), log.getMethod(), log.getLine(), log.getMessage());
			}
		}

	}

	private void sendStats(final Set<Stat> stats, final String uuid, final String route, final long timestamp, final long endDate) {
		Map<String, String> map = null;
		for (Stat stat : this.tokenProvider.getHandler().sortedStats(stats)) {
			if (map == null) {
				map = new HashMap<>();
			}
			map.computeIfAbsent(stat.getService(), k -> stat.getService());
			map.put(stat.getService(), map.get(stat.getService()) + String.format("|%s|%s", stat.getType(), stat.getElapsedMS()));
		}

		if (map != null) {
			for (String value : map.values()) {
				MetricsPostFilter.log.trace(this.logsFormat, this.context.getEnvironment(), Level.TRACE.toString(), this.sdf.format(timestamp), this.sdf.format(endDate), uuid, route, value);
			}
		}
	}
}