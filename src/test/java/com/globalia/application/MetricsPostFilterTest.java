package com.globalia.application;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.globalia.Context;
import com.globalia.HelperTest;
import com.globalia.api.ApiRS;
import com.globalia.enumeration.LogType;
import com.globalia.enumeration.StatType;
import com.globalia.json.JsonHandler;
import com.globalia.monitoring.Log;
import com.globalia.monitoring.Monitor;
import com.globalia.monitoring.MonitorHandler;
import com.globalia.monitoring.Stat;
import com.globalia.security.JwtTokenProvider;
import com.netflix.util.Pair;
import com.netflix.zuul.context.RequestContext;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class MetricsPostFilterTest extends HelperTest {

	@InjectMocks
	private MetricsPostFilter postFilter;
	@Mock
	private MonitorHandler handler;
	@Mock
	private JsonHandler jsonHandler;
	@Mock
	private Context context;
	@Mock
	private JwtTokenProvider tokenProvider;

	private final MockHttpServletRequest request = new MockHttpServletRequest();
	private final MockHttpServletResponse response = new MockHttpServletResponse();
	private ApiRS apiRS;
	private Log log;
	private Stat stat;
	private Monitor monitor;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);

		this.log = new Log();
		this.log.setClazz("clase");
		this.log.setMethod("method");
		this.log.setLine(1);
		this.log.setTimestamp(System.currentTimeMillis());
		this.log.setMessage("");
		this.log.setType(LogType.INFO);
		this.log.setService("");
		this.log.setPort("");
		this.log.setHost("");

		this.stat = new Stat();
		this.stat.setTimestamp(System.currentTimeMillis());
		this.stat.setType(StatType.SQL_QUERY);
		this.stat.setService("");
		this.stat.setPort("");
		this.stat.setHost("");
		this.stat.setElapsedMS(System.currentTimeMillis());

		this.monitor = new Monitor();
		this.monitor.setLogs(new HashSet<>());
		this.monitor.getLogs().add(this.log);
		this.monitor.setStats(new HashSet<>());
		this.monitor.getStats().add(this.stat);

		this.apiRS = new ApiRS();
		this.apiRS.setMonitor(this.monitor);
		this.apiRS.setStatus(200);

		jwtSecret(MetricsPostFilter.class, this.postFilter);
		when(this.tokenProvider.getHandler()).thenReturn(this.handler);
		when(this.context.getEnvironment()).thenReturn("dev");
	}

	@Test
	public void testBasicProperties() {
		assertEquals(99, this.postFilter.filterOrder());
		assertEquals("post", this.postFilter.filterType());
		assertTrue(this.postFilter.shouldFilter());
	}

	@Test
	public void testRunStreamError() throws IOException {
		RequestContext context = RequestContext.getCurrentContext();
		context.setRequest(this.request);
		context.setResponseGZipped(true);
		context.setResponse(this.response);
		context.setResponseDataStream(IOUtils.toInputStream("test\n", StandardCharsets.UTF_8));

		showLogs(MetricsPostFilter.class, this.postFilter);
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenThrow(IOException.class);
		when(this.jsonHandler.toJson(any())).thenReturn("OK");
		when(this.tokenProvider.getHandler().create(anyString(), anyString(), anyString())).thenReturn(this.monitor);
		assertNull(this.postFilter.run());
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), this.response.getStatus());
	}

	@SuppressWarnings("ArraysAsListWithZeroOrOneArgument")
	@Test
	public void testRunStreamAuthorizationHeaderExists() throws IOException {
		RequestContext context = RequestContext.getCurrentContext();
		context.setRequest(this.request);
		context.setResponseGZipped(false);
		context.setResponse(this.response);
		context.setResponseDataStream(IOUtils.toInputStream("test\n", StandardCharsets.UTF_8));
		context.set("zuulResponseHeaders", Arrays.asList(new Pair<>("Authorization", "bearer")));

		showStats(MetricsPostFilter.class, this.postFilter);
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenReturn(this.apiRS);
		when(this.jsonHandler.toJson(any())).thenThrow(JsonProcessingException.class);
		assertNull(this.postFilter.run());
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), this.response.getStatus());
	}

	@SuppressWarnings("ArraysAsListWithZeroOrOneArgument")
	@Test
	public void testRunStreamAuthorizationHeaderExists2() throws IOException {
		RequestContext context = RequestContext.getCurrentContext();
		context.setRequest(this.request);
		context.setResponseGZipped(false);
		context.setResponse(this.response);
		context.setResponseDataStream(IOUtils.toInputStream("test\n", StandardCharsets.UTF_8));
		context.set("zuulResponseHeaders", Arrays.asList(new Pair<>("Authorization", "bearer")));

		showLogs(MetricsPostFilter.class, this.postFilter);
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenReturn(this.apiRS);
		when(this.jsonHandler.toJson(any())).thenThrow(JsonProcessingException.class);
		assertNull(this.postFilter.run());
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), this.response.getStatus());
	}

	@Test
	public void testRunResponseNull() throws IOException {
		RequestContext context = RequestContext.getCurrentContext();
		context.setRequest(this.request);
		context.setResponseGZipped(false);
		context.setResponse(this.response);
		context.setResponseDataStream(IOUtils.toInputStream("test\n", StandardCharsets.UTF_8));

		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenReturn(null);
		assertNull(this.postFilter.run());
		assertEquals(HttpStatus.OK.value(), this.response.getStatus());
	}

	@SuppressWarnings("ArraysAsListWithZeroOrOneArgument")
	@Test
	public void testRunMonitorNull() throws IOException {
		RequestContext context = RequestContext.getCurrentContext();
		this.request.addHeader("Authorization", "bearer");
		context.setRequest(this.request);
		context.setResponseGZipped(false);
		context.setResponse(this.response);
		context.setResponseDataStream(IOUtils.toInputStream("test\n", StandardCharsets.UTF_8));
		context.set("zuulResponseHeaders", Arrays.asList(new Pair<>("XXXXX", "bearer")));

		this.apiRS.setMonitor(null);
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenReturn(this.apiRS);
		when(this.jsonHandler.toJson(any())).thenReturn("OK");
		when(this.tokenProvider.refreshExpiration(anyString(), any(), any())).thenReturn("OK");
		assertNull(this.postFilter.run());
		assertEquals(HttpStatus.OK.value(), this.response.getStatus());
	}

	@SuppressWarnings("ArraysAsListWithZeroOrOneArgument")
	@Test
	public void testRunStream() throws IOException {
		RequestContext context = RequestContext.getCurrentContext();
		this.request.addHeader("Authorization", "bearer");
		context.setRequest(this.request);
		context.setResponseGZipped(false);
		context.setResponse(this.response);
		context.setResponseDataStream(IOUtils.toInputStream("test\n", StandardCharsets.UTF_8));
		context.set("zuulResponseHeaders", Arrays.asList(new Pair<>("XXXXX", "bearer")));

		sendStats(MetricsPostFilter.class, this.postFilter);
		sendLogs(MetricsPostFilter.class, this.postFilter);
		when(this.tokenProvider.getHandler().sortedLogs(this.monitor.getLogs())).thenReturn(Set.of(this.log));
		when(this.tokenProvider.getHandler().sortedStats(this.monitor.getStats())).thenReturn(Set.of(this.stat));
		when(this.jsonHandler.fromJson(anyString(), (Class<?>) any())).thenReturn(this.apiRS);
		when(this.jsonHandler.toJson(any())).thenReturn("OK");
		when(this.tokenProvider.refreshExpiration(anyString(), any(), any())).thenReturn("OK");
		assertNull(this.postFilter.run());
		assertEquals(HttpStatus.OK.value(), this.response.getStatus());
	}
}
