package com.globalia.application;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpServletRequest;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(MockitoJUnitRunner.Silent.class)
public class HttpRQWrapperTest {

	@InjectMocks
	private HttpRQWrapper wrapper;

	@Before
	public void inicializaMocks() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testContentData() {
		assertNotNull(this.wrapper.modifyRequest(new MockHttpServletRequest(), "cinco").getContentData());
	}

	@Test
	public void testReader() throws IOException {
		assertNotNull(this.wrapper.modifyRequest(new MockHttpServletRequest(), "cinco").getReader());
	}

	@Test
	public void testInputStream() throws IOException {
		assertNotNull(this.wrapper.modifyRequest(new MockHttpServletRequest(), "cinco").getInputStream());
	}

	@Test
	public void testContentLengthLong() {
		assertEquals(5L, this.wrapper.modifyRequest(new MockHttpServletRequest(), "cinco").getContentLengthLong());
	}

	@Test
	public void testContentLength() {
		assertEquals(5, this.wrapper.modifyRequest(new MockHttpServletRequest(), "cinco").getContentLength());
	}
}
