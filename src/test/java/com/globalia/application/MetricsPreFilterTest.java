package com.globalia.application;

import com.globalia.monitoring.Monitor;
import com.globalia.monitoring.MonitorHandler;
import com.netflix.zuul.context.RequestContext;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.cloud.netflix.zuul.filters.ZuulProperties;
import org.springframework.mock.web.MockHttpServletRequest;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashSet;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class MetricsPreFilterTest {

	@InjectMocks
	private MetricsPreFilter preFilter;
	@Mock
	private MonitorHandler handler;

	private Monitor monitor;
	private final MockHttpServletRequest request = new MockHttpServletRequest();
	private final ZuulProperties properties = new ZuulProperties();
	private RequestContext ctx;

	@Before
	public void inicializaMocks() throws MalformedURLException {
		MockitoAnnotations.openMocks(this);

		this.ctx = RequestContext.getCurrentContext();
		this.ctx.clear();
		this.ctx.setRequest(this.request);
		this.ctx.set("ignoredHeaders", new HashSet<>(Collections.singletonList("authorization")));
		this.ctx.set("requestURI", "add");
		this.ctx.set("sendZuulResponse", true);
		this.ctx.set("routeHost", new URL("http://test.com:8080/"));

		this.monitor = new Monitor();
		this.monitor.setLogs(new HashSet<>());
		this.monitor.setStats(new HashSet<>());
	}

	@Test
	public void testBasicProperties() {
		assertEquals(6, this.preFilter.filterOrder());
		assertEquals("pre", this.preFilter.filterType());
	}

	@Test
	public void testSkipShouldFilter() {
		assertFalse(this.preFilter.shouldFilter());
	}

	@Test
	public void testSkipShouldFilter2() {
		this.request.setRequestURI("/foo/1");
		assertFalse(this.preFilter.shouldFilter());
	}

	@Test
	public void testShouldFilter() {
		this.request.setRequestURI("/api/1");
		assertTrue(this.preFilter.shouldFilter());
	}

	@Test
	public void testRunNullPointer() {
		this.properties.setPrefix("/api");
		this.properties.setSensitiveHeaders(Collections.singleton("authorization"));
		this.request.setRequestURI("/api/foo/add");
		this.request.addHeader("authorization", "bearer");

		assertNull(this.preFilter.run());
	}

	@Test
	public void testRunWithOutBody() {
		this.properties.setPrefix("/api");
		this.properties.setSensitiveHeaders(Collections.singleton("authorization"));
		this.request.setRequestURI("/api/foo/add");
		this.request.addHeader("authorization", "bearer");

		when(this.handler.create("", null, null)).thenReturn(this.monitor);
		assertNull(this.preFilter.run());
	}

	@Test
	public void testRunBodyNull() {
		this.properties.setPrefix("/api");
		this.properties.setSensitiveHeaders(Collections.singleton("authorization"));
		this.request.setRequestURI("/api/foo/add");
		this.request.addHeader("authorization", "bearer");
		this.ctx.set("requestEntity", null);

		when(this.handler.create("", null, null)).thenReturn(this.monitor);
		assertNull(this.preFilter.run());
	}

	@Test
	public void testRun() {
		this.properties.setPrefix("/api");
		this.properties.setSensitiveHeaders(Collections.singleton("authorization"));
		this.request.setRequestURI("/api/foo/add");
		this.request.addHeader("authorization", "bearer");

		InputStream stream = new ByteArrayInputStream("{}".getBytes(StandardCharsets.UTF_8));
		this.ctx.set("requestEntity", stream);
		this.ctx.set("proxy", "test_test");

		when(this.handler.create("{}", null, null)).thenReturn(this.monitor);
		assertNull(this.preFilter.run());
	}
}
