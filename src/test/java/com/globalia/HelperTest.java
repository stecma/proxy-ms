package com.globalia;

import java.lang.reflect.Field;

public abstract class HelperTest {

	public static void jwtSecret(final Class<?> classes, final Object obj) {
		try {
			Field field = classes.getDeclaredField("jwtSecret");
			field.setAccessible(true);
			field.set(obj, "JwtSecret0123456");
		} catch (NoSuchFieldException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	public static void showLogs(final Class<?> classes, final Object obj) {
		try {
			Field field = classes.getDeclaredField("showLogs");
			field.setAccessible(true);
			field.set(obj, true);
		} catch (NoSuchFieldException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	public static void showStats(final Class<?> classes, final Object obj) {
		try {
			Field field = classes.getDeclaredField("showStats");
			field.setAccessible(true);
			field.set(obj, true);
		} catch (NoSuchFieldException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	public static void sendStats(final Class<?> classes, final Object obj) {
		try {
			Field field = classes.getDeclaredField("sendStats");
			field.setAccessible(true);
			field.set(obj, true);
		} catch (NoSuchFieldException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	public static void sendLogs(final Class<?> classes, final Object obj) {
		try {
			Field field = classes.getDeclaredField("sendLogs");
			field.setAccessible(true);
			field.set(obj, true);
		} catch (NoSuchFieldException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}
}